# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools

class sds(models.Model):
    _name = 'elektromery.sds'
    _order = 'name'

    name = fields.Char()
    devid = fields.Char()
    url = fields.Html('URL')

class elektromery(models.Model):
    _name = 'elektromery.elektromery'
    _description = 'elektromery.elektromery'
    _order = 'cislo,name'

    name = fields.Char()
    sds = fields.Many2one('elektromery.sds', 'Zařízení')
    devname = fields.Char()
    devid = fields.Char(related='sds.devid')
    impulse = fields.Integer(string="Impulsy", default=800, required=True)
    ratio = fields.Integer(string="Převodový poměr", default=1, required=True)
    lastwh = fields.Float(compute="_lastwh", store=False, string="kWh")
    lasttime = fields.Datetime(compute="_lastwh", store=False)
    wh_ids = fields.One2many('elektromery.data', 'elektromer', readonly=True)
    parent_id = fields.Many2one('elektromery.elektromery', 'Nadřazený')
    child_ids = fields.One2many('elektromery.elektromery', 'parent_id')
    cislo = fields.Integer(string="Číslo")
    description = fields.Text()
    active = fields.Boolean(default=True)
    @api.depends('wh_ids')
    def _lastwh(self):
        self.env.cr.execute("SELECT meter,last(wh,time)::float * e.ratio / e.impulse AS wh, last(time,time) AS time FROM emeter AS m JOIN elektromery_elektromery AS e ON m.meter = e.id WHERE meter = ANY(%s) GROUP BY meter, e.id", (self.ids,) )
        m = {k:(w,t.strftime("%Y-%m-%d %H:%M:%S")) for k,w,t in self.env.cr.fetchall()}
        for record in self:
            record.lastwh, record.lasttime = m.get(record.id)


# /datas0?devid=MfzpDpifNHSlxjGCYBobVidpcnNXwM&t0v1=0&t0v2=0&t0v3=0&t0v4=0&t0v5=0&t0v6=0&t0v7=0&t0v8=0&t1v1=0&t1v2=0&t1v3=0&t1v4=0&t1v5=0&t1v6=0&t1v7=0&t1v8=0&av1=0&av2=0&av3=0&av4=0&av5=0&av6=0&av7=0&av8=0&tt=0&time1694150820
class data(models.Model):
    _name = 'elektromery.data'
    _order = 'elektromer, date desc'
    _auto = False
    _log_access = True # Include magic fields

    elektromer = fields.Many2one('elektromery.elektromery')
    sds = fields.Many2one('elektromery.sds', 'Zařízení')
    date = fields.Datetime(required=True)
    wh = fields.Float(required=True, string="kWh")
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
#        self.env.cr.execute("""CREATE or REPLACE VIEW %s AS
#            SELECT ((m.ctid::text::point)[0]::bigint << 32) | (m.ctid::text::point)[1]::bigint AS id, time as date, meter as elektromer, wh, time as create_date, time as write_date, 1 AS create_uid, 1 AS write_uid, e.sds
#            FROM emeter AS m JOIN elektromery_elektromery AS e ON (m.meter = e.id)""" % (self._table,))
        self.env.cr.execute("""CREATE or REPLACE VIEW %s AS
            SELECT (extract(epoch from time)::bigint-1674630000)*1000+meter AS id, time as date, meter as elektromer, wh::float*e.ratio/e.impulse AS wh, time as create_date, time as write_date, 1 AS create_uid, 1 AS write_uid, e.sds
            FROM emeter AS m JOIN elektromery_elektromery AS e ON (m.meter = e.id) WHERE e.active""" % (self._table,))


class hodnoty(models.Model):
    _name = 'elektromery.hodnoty'
    _description = 'elektromery.hodnoty'
    _order = 'elektromer, date'
    _auto = False
    _log_access = True # Include magic fields

    elektromer = fields.Many2one('elektromery.elektromery')
    sds = fields.Many2one('elektromery.sds', 'Zařízení')
    date = fields.Datetime(required=True)
    wh = fields.Float(required=True, string="kWh")
#    create_uid = fields.Many2one('res.users', readonly=True)
#    create_date = fields.Date(readonly=True)
#    write_uid = fields.Many2one('res.users', readonly=True)
#    write_date = fields.Date(readonly=True)
    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
#        self.env.cr.execute("""CREATE or REPLACE VIEW %s AS
#            SELECT ((m.ctid::text::point)[0]::bigint << 32) | (m.ctid::text::point)[1]::bigint AS id, time as date, meter as elektromer, wh - lag(wh, 1, 0) OVER (PARTITION BY meter ORDER by time) AS wh, time as create_date, time as write_date, 1 AS create_uid, 1 AS write_uid, e.sds
#            FROM emeter AS m JOIN elektromery_elektromery AS e ON (m.meter = e.id) WHERE e.active""" % (self._table,))
        self.env.cr.execute("""CREATE or REPLACE VIEW %s AS
            SELECT (extract(epoch from time)::bigint-1674630000)*1000+meter AS id, time AS date, meter as elektromer, (wh - lag(wh, 1, wh) OVER (PARTITION BY meter ORDER by time))::float*e.ratio/e.impulse AS wh, time AS create_date, time AS write_date, 1 AS create_uid, 1 AS write_uid, e.sds
            FROM emeter AS m JOIN elektromery_elektromery AS e ON (m.meter = e.id) WHERE e.active""" % (self._table,))



#WITH data AS (select date_trunc('month', time) AS day,meter, last(wh,time) as lwh,  last(wh,time) - first(wh,time) as dwh from emeter where meter=1 group by day,meter) select day,meter,lwh,dwh,lwh - lag(lwh) over (order by day) FROM data;
