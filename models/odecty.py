# -*- coding: utf-8 -*-

from odoo import api, fields, models, tools

class odecty(models.Model):
    _name = 'elektromery.odecty'
    _inherit = ['portal.mixin','mail.thread','mail.activity.mixin']

    meter = fields.Many2one('elektromery.elektromery', required=True)
    date = fields.Datetime(string="Datun", required=False, states={'done': [('required', True)],})
    value = fields.Float(string="Stav", required=False, states={'done': [('required', True)],} )
    responsibility = fields.Many2one('res.partner','Osoba zodpovědná', required=True, default=lambda self: self.env.user)
    foto = fields.Image()
    state = fields.Selection([
        ('draft', 'Požadavek na ruční odečet'),
        ('todo', 'Seřídit'),
        ('done', 'Odečteno')
        ], string='State', readonly=False, tracking=True, copy=False, default='draft',
        help="")
    def button_draft(self):
        self.sudo().write({'state': 'draft', })
    def button_done(self):
        self.sudo().write({'state': 'done', })

