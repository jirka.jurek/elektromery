# -*- coding: utf-8 -*-
from odoo import http
from psycopg2 import extras
import datetime
#import pytz

import logging
_logger = logging.getLogger(__name__)



class ElektromeryController(http.Controller):
    @http.route('/datas0', auth='public')
    def index(self, **kw):
        d = http.request.get_http_params()
        res = ""
        t = datetime.datetime.now()
#        tz = pytz.timezone('CET')
        for k in d:
            if k.startswith('time'):
                try:
                    t = datetime.datetime.fromtimestamp(int(k[4:])) - datetime.timedelta(hours=2)
                except:
                    pass
        res = []
        devid = d.get('devid')
        if not devid:
            return "no devid"

        sds = http.request.env['elektromery.sds'].sudo().search([('devid','=',devid)])
        if not sds:
            sds = sds.create([{'devid': devid,}])
        for i in range(1,9):
            devname = 't0v%d' % i
            v = d.get(devname)
            try:
                v = int(v)
            except:
                continue
            http.request.cr.execute("SELECT e.id FROM elektromery_elektromery AS e JOIN elektromery_sds AS s ON e.sds = s.id WHERE s.devid = %s AND e.devname = %s", (devid, devname))
            x = http.request.cr.fetchone()
            eid = None
            if x:
                eid = x[0]
#            e = http.request.env['elektromery.elektromery'].sudo().search(['&',('devid','=',devid),('devname','=',devname),"|",("active", "=", True),("active", "=", False)])
            if not eid:
                e = e.create([{'devname': devname, 'sds': sds.id, 'name': devname}])
                eid = e.id
            res.append((t, eid, v))

        _logger.info(res)
        extras.execute_values(http.request.env.cr, "INSERT INTO emeter(time,meter,wh) VALUES %s ON CONFLICT DO NOTHING", res)
        return "Ok"

    @http.route('/elektromery/elektromery/objects', auth='public')
    def list(self, **kw):
        return http.request.render('elektromery.listing', {
            'root': '/elektromery/elektromery',
            'objects': http.request.env['elektromery.elektromery'].search([]),
        })

    @http.route('/elektromery/elektromery/objects/<model("elektromery.elektromery"):obj>', auth='public')
    def object(self, obj, **kw):
        return http.request.render('elektromery.object', {
            'object': obj
        })
